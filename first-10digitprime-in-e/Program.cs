﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace first_10digitprime_in_e
{
    class Program
    {
        static decimal e;
        static bool silniaOverflow = false;

        static decimal silnia(decimal x)
        {
            if (x == 0) return 1;
            else
            {
                decimal iterator=1 ;
                decimal silnia=1;
                try
                {
                    while (iterator <= x)
                    {
                        silnia *= iterator;
                        iterator++;
                    }
                }
                catch
                {
                    //   Console.WriteLine("zakres silni przekroczony");
                    silniaOverflow = true;
                    return silnia;
                }
                return silnia;
            }
        }
        static decimal approximateE()
        {
            decimal approximation=0;
            decimal iteration=0;
            try
            {
                while (!silniaOverflow)
                {
                    approximation += 1 / silnia(iteration);
                    iteration++;
                }
            }
            catch
            {
                return approximation;
            }
            return approximation;
        }
        static void Main(string[] args)
        {
            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalDigits = 60;
            e = approximateE();
            Console.WriteLine(e.ToString("N", nfi));
          //  decimal s = silnia(28);
         //   Console.WriteLine(s);
            Console.ReadKey();
        }
    }
}
